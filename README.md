# aur_cli

[![Crates.io](https://img.shields.io/crates/v/fus.svg)](https://crates.io/crates/aur_cli)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/aur_cli)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/aur_cli/-/raw/master/LICENSE)

`aur_cli` is a CLI that produces a release tarball and PKGBUILD file for a Rust project, so that it can be released on the Arch Linux User Repository (AUR).

No extra configuration is necessary. As long as your `Cargo.toml` has [the usual
fields](https://rust-lang.github.io/api-guidelines/documentation.html#c-metadata),
a PKGBUILD will be generated with all the necessary sections filled out.

## Installation

Guess what? `aur_cli` itself is on the AUR! Install it with an AUR-compatible

```
yay -S aur_cli
```

... or via `cargo`:

```
cargo install aur_cli
```

## Usage

### Basics

Navigate to a Rust project, and run:

```
aur_cli
```

This will produce a `foobar-1.2.3-x86_64.tar.gz` tarball and a PKGBUILD.

If you wish, you can now run `makepkg` to ensure that your package actually builds.

```
> makepkg
==> Making package: aur_cli-bin 1.0.0-1 (Wed 10 Jun 2020 08:23:46 PM PDT)
==> Checking runtime dependencies...
==> Checking buildtime dependencies...
... etc ...
==> Finished making: aur_cli 1.0.0-1 (Wed 10 Jun 2020 08:23:47 PM PDT)
```


At this point, it is up to you to:

1. Create an official `Release` on Github/Gitlab, attaching the original binary
   tarball that `aur_cli` produced.
2. Copy the PKGBUILD to a git repo that tracks releases of your package.
3. Run `makepkg --printsrcinfo > .SRCINFO`.
4. Commit both files and push to the AUR.

Some of these steps may be automated in `aur_cli` at a later date if there is
sufficient demand.

### Custom Binary Names

If you specify a `[[bin]]` section in your `Cargo.toml` and set the `name`
field, this will be used as the binary name to install within the PKGBUILD.

### `depends` and `optdepends`

If your package requires other Arch packages at runtime, you can specify these
within your `Cargo.toml` like this:

```toml
[package.metadata]
depends = ["nachos", "pizza"]
optdepends = ["sushi", "ramen"]
```

And these settings will be copied to your PKGBUILD.

### Static Binaries

Run with `--musl` to produce a release binary that is statically linked via
[MUSL](https://musl.libc.org/).

```
> aur_cli --musl
> cd target/x86_64-unknown-linux-musl/release/
> ldd <your-binary>
    not a dynamic executable
```
